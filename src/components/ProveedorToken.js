import React from "react";
import useLocalStorage from "../hooks/useLocalStorage";

const ContextoToken = React.createContext();

const ProveedorToken = (props) => {
  const [token, setToken] = useLocalStorage("authToken", "");

  return (
    <ContextoToken.Provider value={[token, setToken]}>{props.children}</ContextoToken.Provider>
  );
};

export { ContextoToken, ProveedorToken };
