import { Container } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import EditarNombre from "./EditarNombre";
import EditarEmail from "./EditarEmail";
import EditarContrasena from "./EditarContrasena";

//import "./FormularioRegistro.css"
import "../style/EstilosComponente/usuario.css";

const Usuario = (props) => {
  const { nombre, email, rol } = props.children;

  return (
    <Container>
      <Row>
        <Col sm={12}>Datos del Usuario </Col>
      </Row>
      <Row>
        <Col sm>
          <EditarNombre>{nombre}</EditarNombre>
        </Col>
      </Row>
      <Row>
        <Col sm>{nombre}</Col>
      </Row>
      <Row>
        <Col sm>
          <EditarEmail>{email}</EditarEmail>
          <Col sm>{email}</Col>
        </Col>
      </Row>
      <Row>
        <Col sm>Rol</Col>
        <Col sm>{rol}</Col>
      </Row>
      <Row>
        <Col sm>
          <EditarContrasena></EditarContrasena>
        </Col>
      </Row>
    </Container>
  );
};

export default Usuario;
