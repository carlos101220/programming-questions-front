import { useState, useContext } from "react";
import { ContextoToken } from "./ProveedorToken";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "./FormularioRegistro.css";
import { useHistory } from "react-router-dom";

const FormularioRegistro = (props) => {
  const history = useHistory();
  const [nombre, setNombre] = useState("");
  const [apellidos, setApellidos] = useState("");
  const [email, setEmail] = useState("");
  const [contrasena, setContrasena] = useState("");
  const [rol, setRol] = useState("estudiante");

  const [etiquetas, setEtiquetas] = useState("");
  const [, setToken] = useContext(ContextoToken);
  const [error, setError] = useState("");

  const registro = async (e) => {
    e.preventDefault();
    const res = await fetch("http://localhost:3050/usuario", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre,
        apellidos,
        email,
        contrasena,
        rol,
        tecnologiaLenguajes: etiquetas,
      }),
    });
    console.log(res);

    if (res.ok) {
      const data = await res.json();
      setError("");
      setToken(data.accessToken);
      redireccionar();
    } else {
      setError();
    }
  };

  const redireccionar = () => {
    history.push("/");
  };

  function validateForm() {
    return nombre.length > 0 && email.length > 0 && contrasena.length > 0;
  }

  return (
    <div className="Registro">
      <Form id="registro" onSubmit={registro}>
        <Form.Group size="lg">
          <Form.Label>Nombre</Form.Label>
          <Form.Control
            autoFocus
            type="text"
            name="nombre"
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
          />
        </Form.Group>

        <Form.Group size="lg">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="text"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>

        <Form.Group size="lg">
          <Form.Label>Contraseña</Form.Label>
          <Form.Control
            type="password"
            name="contrasena"
            value={contrasena}
            onChange={(e) => setContrasena(e.target.value)}
          />
        </Form.Group>

        <Form.Group size="lg">
          <Form.Label>Rol</Form.Label>
          <Form.Control
            name="registroRol"
            as="select"
            defaultValue="estudiante"
            onClick={(e) => {
              setRol(e.target.value);
            }}
          >
            <option value="estudiante">Estudiante</option>
            <option value="experto">Experto</option>
          </Form.Control>
        </Form.Group>

        {rol === "experto" ? (
          <>
            <Form.Group size="lg">
              <Form.Label>
                Tecnologias/Lenguajes en las que eres expereto (minimo 1 y maximo 5).
              </Form.Label>
              <Form.Control
                type="text"
                name="registroEtiqueta"
                value={etiquetas}
                placeholder="ejemplo: java, c#, react"
                onChange={(e) => setEtiquetas(e.target.value)}
              />
            </Form.Group>
          </>
        ) : (
          ""
        )}
        {/* <Form.Group size="lg" controlId="registroRol">
          <Form.Label htmlFor="registroRol">Seleccionar Rol</Form.Label>
          <Form.Control
            type="text"
            name="rol"
            value={rol}
            onChange={(e) => setRol(e.target.value)}
          ></Form.Control>
        </Form.Group> */}

        <Button variant="dark" block size="lg" type="submit" disabled={!validateForm()}>
          Registrarse
        </Button>
        {error && <p style={{ color: "red" }}>{error}</p>}
      </Form>
    </div>
  );
};

export default FormularioRegistro;
