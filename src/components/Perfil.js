import { useEffect } from "react";
import useCargarUsuarios from "../hooks/useCargarUsuarios";
import Usuario from "./Usuario";

const ListarUsuarios = (props) => {
  const id = localStorage.getItem("id");

  const [usuarios] = useCargarUsuarios(id);

  const arrayUsuarios = usuarios?.map((contenido) => (
    <Usuario id="usuario" className="usuario" key={contenido.id}>
      {contenido}
    </Usuario>
  ));

  return (
    <div id="contenido">
      <section>
        <nav>
          <ul>{arrayUsuarios}</ul>
        </nav>
      </section>
    </div>
  );
};

export default ListarUsuarios;
