import { useState, useContext } from "react";
import { ContextoToken } from "./ProveedorToken";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "./FormularioLogin.css";

const FormularioLogin = (props) => {
  const [email, setEmail] = useState("");
  const [contrasena, setPassword] = useState("");
  const [token, setToken] = useContext(ContextoToken);
  const [error, setError] = useState("");

  const login = async (e) => {
    e.preventDefault();
    const res = await fetch("http://localhost:3050/iniciarSesion", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email, contrasena }),
    });
    const data = await res.json();
    if (res.ok) {
      setError("");
      localStorage.setItem("id", `${data.id}`);
      localStorage.setItem("rol", `${data.rol}`);
      localStorage.setItem("nombre", `${data.nombre}`);
      setToken(`${data.token}`);
    } else {
      alert(data.error);
    }

    //const idGet = localStorage.getItem('id');

    // console.log(idGet);
  };

  return (
    <div className="Login">
      <Form id="login" onSubmit={login}>
        <Form.Group size="lg" controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            autoFocus
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group size="lg" controlId="contrasena">
          <Form.Label>Contraseña</Form.Label>
          <Form.Control
            type="password"
            value={contrasena}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        <Button variant="dark" block size="lg" type="submit">
          Login
        </Button>
      </Form>
    </div>
  );
};

export default FormularioLogin;
