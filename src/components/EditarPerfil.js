import { useEffect, useState } from "react";
import useCargarUsuarios from "../hooks/useCargarUsuarios";
import { ContextoToken } from "./ProveedorToken";
import { useContext } from "react";
import { Redirect } from "react-router-dom";
const EditarPefil = (props) => {
  const [usuario] = useCargarUsuarios();

  const [token] = useContext(ContextoToken);
  const [nombre, setNombre] = useState(usuario[0]?.nombre);

  const [email, setEmail] = useState("");
  const [contrasena, setContrasena] = useState("");
  const [rol, setRol] = useState("");

  const actualizar = async (e) => {
    const res = await fetch(`http://localhost:3050/usuario/editar/${localStorage.getItem("id")}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        autorizado: token,
      },
      body: JSON.stringify({ nombre }),
    });
    console.log(res);
  };

  useEffect(() => {}, [nombre]);

  function validateForm() {
    return nombre.length > 0 && email.length > 0 && contrasena.length > 0;
  }

  return (
    <>
      {token ? (
        <div className="actualizar">
          <form>
            <section>
              <label htmlFor="nombre">Nombre</label>
              <input
                type="text"
                name="nombre"
                id="nombre"
                value={nombre}
                onChange={(e) => {
                  setNombre(e.target.value);
                }}
              ></input>
              <button onClick={actualizar}>Editar</button>
            </section>
            <section>
              <label htmlFor="email">Email</label>
              <input
                type="text"
                name="email"
                id="email"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              ></input>
              <button onClick={actualizar}>Editar</button>
            </section>
          </form>
        </div>
      ) : (
        <>
          <Redirect to="/"></Redirect>
        </>
      )}
    </>
  );
};

export default EditarPefil;
