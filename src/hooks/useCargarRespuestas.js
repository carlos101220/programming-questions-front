import { useEffect, useState } from "react";
import { useParams } from "react-router";

const useCargarRespuestas = () => {
  const [preguntaMasRespuestas, setPreguntaMasRespuestas] = useState([]);
  const { id } = useParams();

  const cargarRespuestas = async () => {
    const res = await fetch(`http://localhost:3050/pregunta/${id}/respuesta`);
    const json = await res.json();
    setPreguntaMasRespuestas(json);
  };
  useEffect(() => {
    cargarRespuestas();

    const intervalo = setInterval(cargarRespuestas, 500);

    return () => {
      setPreguntaMasRespuestas([]);
      clearInterval(intervalo);
    };
  }, []);

  return [preguntaMasRespuestas];
};

export default useCargarRespuestas;
