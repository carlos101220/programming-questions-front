import { useParams } from "react-router-dom";
import { useContext } from "react";
import { ContextoToken } from "../components/ProveedorToken";

const useEditarPublicacion = (texto) => {
  const [token] = useContext(ContextoToken);

  const { id } = useParams();
  const editarPregunta = async () => {
    const res = await fetch(`http://localhost:3050/pregunta/respuesta/${id}`, {
      method: "PUT",
      header: {
        "Content-Type": "application/json",
        autorizado: token,
      },
      body: JSON.stringify({ texto }),
    });
    const json = await res.json();
  };
  editarPregunta();
};

export default useEditarPublicacion;
