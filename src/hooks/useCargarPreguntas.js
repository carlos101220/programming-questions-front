import { useEffect, useState } from "react";

const useCargarPreguntas = () => {
  const [preguntas, setPreguntas] = useState([]);

  useEffect(() => {
    const CargarPreguntas = async () => {
      const res = await fetch(`http://localhost:3050/pregunta?buscar=&filtro=&sinRespuestas=`);
      const json = await res.json();

      console.log(json);
      setPreguntas(json);
    };

    CargarPreguntas();

    const intervalo = setInterval(CargarPreguntas, 1000);

    return () => {
      setPreguntas([]);
      clearInterval(intervalo);
    };
  }, []);

  return [preguntas];
};

export default useCargarPreguntas;
