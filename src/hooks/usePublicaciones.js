import { useEffect, useState } from "react";
import { useContext } from "react";
import { ContextoToken } from "../components/ProveedorToken";

const usePublicaciones = () => {
  const [token] = useContext(ContextoToken);
  const [publicaciones, setPublicaciones] = useState([]);

  useEffect(() => {
    const CargarPreguntas = async () => {
      const res = await fetch(`http://localhost:3050/perfil/publicaciones`, {
        headers: {
          autorizado: token,
        },
      });
      const json = await res.json();

      setPublicaciones(json);
    };

    CargarPreguntas();

    const intervalo = setInterval(CargarPreguntas, 1000);

    return () => {
      setPublicaciones([]);
      clearInterval(intervalo);
    };
  }, []);

  return [publicaciones];
};

export default usePublicaciones;
