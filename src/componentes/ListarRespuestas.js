import { useState } from "react";
import { useParams } from "react-router";
import useCargarRespuestas from "../hooks/useCargarRespuestas";
import Respuestas from "./Respuestas";
import "../style/EstilosComponente/listarRespuestas.css";
import { useContext } from "react";
import { ContextoToken } from "../components/ProveedorToken";
import { Form } from "react-bootstrap";
import { Col } from "react-bootstrap";
import { Card } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
const ListarRespuestas = (props) => {
  const [token] = useContext(ContextoToken);
  const [preguntaMasRespuestas] = useCargarRespuestas();
  const [textoRespuesta, setTextoRespuesta] = useState("");
  const { id } = useParams();

  const {
    titulo,
    textoPregunta,
    fechaActualizacionPregunta,
    fechaPublicacionPregunta,
    listasDeRespuestas,
    etiqueta,
    nombre,
  } = preguntaMasRespuestas;

  const fechaActualizacion = new Date(fechaActualizacionPregunta);
  const fechaCreacion = new Date(fechaPublicacionPregunta);

  const arrayEtiquetas = etiqueta?.split(",").map((tags, index) => (
    <li key={index} className="etiqueta">
      <Link to={`/pregunta/?buscar=&etiqueta=${tags}&sinRespuestas=`}>{tags}</Link>
    </li>
  ));

  const arrayRespuestas = listasDeRespuestas?.map((respuesta) => (
    <Respuestas key={respuesta.id}>{respuesta}</Respuestas>
  ));

  const PublicarRespuestas = async (e) => {
    const res = await fetch(`http://localhost:3050/pregunta/${id}/respuesta`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        autorizado: token,
      },
      body: JSON.stringify({ texto: textoRespuesta }),
    });
    const json = await res.json();
    if (res.ok) {
      alert("has creado la publicacion");
      setTextoRespuesta("");
    } else {
      alert(json.error);
    }
  };

  return (
    <>
      <div class="pagina_respuesta">
        <div class="container">
          <div class="row">
            <div class="col-md">
              <Form>
                <Form.Group as={Col} controlId="tituloPregunta">
                  <h1>{titulo}</h1>

                  <Card>{textoPregunta}</Card>
                  <div>
                    <div>
                      <p>Etiquetas:</p>
                    </div>
                    <ol>{arrayEtiquetas}</ol>
                  </div>
                  <section className="infoPregunta">
                    <div>
                      <section>
                        <p>{`Formulada por: ${nombre}`}</p>
                      </section>
                    </div>
                    <div>
                      <time>{fechaActualizacion.toLocaleString()}</time>
                    </div>
                    <div>
                      <time>{fechaCreacion.toLocaleString()}</time>
                    </div>
                  </section>
                </Form.Group>
                <Form.Group>
                  <section className="arrayRespuestas">
                    {token ? (
                      <>
                        <nav>
                          <ul>{arrayRespuestas}</ul>
                        </nav>
                      </>
                    ) : (
                      <>
                        <nav></nav>
                      </>
                    )}
                  </section>
                </Form.Group>
                <Form.Group as={Col} controlId="contenidoPregunta">
                  <h1>Cuerpo</h1>
                  <Form.Label>
                    Incluye toda los datos necesarios para responder tu pregunta
                  </Form.Label>
                  <Form.Control
                    as="textarea"
                    name="textarea_respuesta"
                    rows="20"
                    cols="100"
                    placeholder="Escribe con detalle tu consulta"
                    value={textoRespuesta}
                    onChange={(e) => setTextoRespuesta(e.target.value)}
                    required
                    type="text"
                    id="inpuTitulo"
                    maxLength="500"
                    style={{ height: "300px" }}
                  ></Form.Control>
                </Form.Group>

                <Button as={Col} type="button" onClick={PublicarRespuestas}>
                  Publicar respuestas
                </Button>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ListarRespuestas;
