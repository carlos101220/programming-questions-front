import Buscador from "./Buscador";
import { Link } from "react-router-dom";
import { useContext } from "react";
import { ContextoToken } from "../components/ProveedorToken";
import { Navbar } from "react-bootstrap";
import { Nav } from "react-bootstrap";
import { NavDropdown } from "react-bootstrap";
import { Button } from "react-bootstrap";

const Cabecera = () => {
  const [token] = useContext(ContextoToken);

  return (
    <>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="/">Programming Questions</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/">Inicio</Nav.Link>
            {localStorage.getItem("rol") === "estudiante" ? (
              <>
                <Nav.Link href="/publicarPregunta">Formular pregunta</Nav.Link>
              </>
            ) : (
              ""
            )}
            <NavDropdown title="Mas Opciones" id="basic-nav-dropdown">
              <NavDropdown.Item href="/perfil/publicaciones">Tus Publicaciones</NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Buscador></Buscador>

          <section>
            {!token ? (
              <>
                <section className="contenedor_link">
                  <Button variant="dark" className="link">
                    <Link to="/login">Iniciar Sesión</Link>
                  </Button>

                  <Button variant="dark" className="link">
                    <Link to="/registro">Registrarse</Link>
                  </Button>
                </section>
              </>
            ) : (
              <>
                <section>
                  <p>{`Bienvenido, ${localStorage.getItem("nombre")}`}</p>
                </section>
                <section className="contenedor_link">
                  <Button
                    variant="dark"
                    className="contenedor_link"
                    href="/"
                    onClick={(e) => {
                      localStorage.clear();
                      window.location.reload();
                    }}
                  >
                    Cerrar Sesión
                  </Button>
                  <Button variant="dark" className="contenedor_link">
                    <Link to="/Perfil">Perfil</Link>
                  </Button>
                </section>
              </>
            )}
          </section>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default Cabecera;
