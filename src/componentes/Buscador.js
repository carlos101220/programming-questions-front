import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { Button } from "react-bootstrap";
import { Form } from "react-bootstrap";
import { FormControl } from "react-bootstrap";
import { InputGroup } from "react-bootstrap";

const Buscador = () => {
  const [inputBuscador, setInputBuscador] = useState("");
  const [sinRespuestas, setSinRespuestas] = useState("");
  const [etiquetas, setEtiquetas] = useState("");
  const history = useHistory();

  useEffect(() => {
    redireccionar();
    if (!sinRespuestas && !inputBuscador) {
      history.push(`/`);
    }
  }, [sinRespuestas, inputBuscador]);

  const redireccionar = () => {
    if (inputBuscador || sinRespuestas) {
      history.push(
        `/pregunta/?buscar=${inputBuscador}&etiqueta=${etiquetas}&sinRespuestas=${sinRespuestas}`
      );
    }
  };

  return (
    <>
      <Form inline>
        <FormControl
          type="text"
          placeholder="Buscar"
          className="buscador"
          name="buscar"
          id="buscar"
          value={inputBuscador}
          onChange={(e) => {
            setInputBuscador(e.target.value);
          }}
        />

        <Button
          variant="outline-success"
          to={
            inputBuscador || sinRespuestas
              ? `/buscar=${inputBuscador}&filtro=&sinRespuestas=${sinRespuestas}`
              : "/"
          }
        >
          Search
        </Button>

        <InputGroup className="checkbox_sinrespeustas">
          <label name="sin_respuestas">Preguntas sin respuestar</label>
          <Form.Check
            type="switch"
            name="sin_respuestas"
            id="sin_respuestas"
            onChange={(e) => setSinRespuestas(e.target.checked)}
          />
        </InputGroup>
      </Form>
    </>
  );
};

export default Buscador;
