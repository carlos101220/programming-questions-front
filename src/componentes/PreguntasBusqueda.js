import { Link } from "react-router-dom";
import "../style/EstilosComponente/pregunta.css";

const PreguntaBusqueda = (props) => {
  const { id, titulo, etiqueta } = props.children;
  const array = etiqueta?.split(",");
  const arrayEtiquetas = array.map((tags, index) => (
    <>
      <li key="index" className="etiqueta">
        {tags}
      </li>
    </>
  ));

  return (
    <li>
      <section className="cubo_pregunta">
        <section className="">
          <article>
            <Link className="Link" to={`/pregunta/${id}`}>
              <h2 className="preguntaID">{titulo}</h2>
            </Link>
          </article>
        </section>
        <section></section>
        <section>
          <nav>
            <ul>{arrayEtiquetas}</ul>
          </nav>
        </section>
      </section>
    </li>
  );
};

export default PreguntaBusqueda;
