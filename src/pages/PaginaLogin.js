import LoginForm from "../components/FormularioLogin";
import { useContext } from "react";
import { ContextoToken } from "../components/ProveedorToken";
import { Redirect, Link } from "react-router-dom";
import "../style/EstilosPagina/PaginaLogin.css";

const PaginaLogin = (props) => {
  const [token] = useContext(ContextoToken);

  return (
    <>
      {!token ? (
        <div class="principal">
          <div>
            <h2 style={{ fontWeight: "600" }}>Inicio de Sesión</h2>
          </div>
          <LoginForm />
          <div>

          <p>
            <Link to="/recuperarContraseña" style={{ fontWeight: "bold" }}>
              ¿Se te olvidó tu contraseña?
            </Link>
          </p>
          <p>
            ¿No tienes cuenta?
            <Link to="/registro" style={{ fontWeight: "bold" }}>
              Regístrate
            </Link>
          </p>
          </div>
        </div>
      ) : (
        <Redirect to="/" />
      )}
    </>
  );
};

export default PaginaLogin;

