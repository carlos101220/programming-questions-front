import { useState } from "react";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import "../components/FormularioLogin.css";

const RescuperarContraseña = (props) => {
  const [email, setEmail] = useState("");

  const [error, setError] = useState("");

  const login = async (e) => {
    e.preventDefault();
    const res = await fetch("http://localhost:3050/recuperar", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email }),
    });
    const data = await res.json();
    if (res.ok) {
      setError("");
      alert(data);
    } else {
      alert(data.error);
    }

    //const idGet = localStorage.getItem('id');

    // console.log(idGet);
  };

  return (
    <div className="Login">
      <Form id="login" onSubmit={login}>
        <Form.Group size="lg" controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            autoFocus
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Button variant="dark" block size="lg" type="submit">
          <Link to="/">Recuperar contraeña</Link>
        </Button>
      </Form>
    </div>
  );
};

export default RescuperarContraseña;
