import Perfil from "../components/Perfil";
import { Link, Redirect } from "react-router-dom";
import { Button } from "react-bootstrap";
import "../style/EstilosPagina/paginaUsuario.css";

const PaginaUsuario = () => {
  return (
    <>
      <>
        <header></header>
        <main className="contenido">
          <section></section>
          <div>
            <Perfil />
          </div>
          <div class="boton">
            <Button href="/perfil/editar" variant="dark" block size="lg">
              Editar perfil
            </Button>
          </div>

          <section>
            <Link to="/perfil/publicaciones">Editar publicaciones</Link>
          </section>
        </main>
      </>
    </>
  );
};

export default PaginaUsuario;
