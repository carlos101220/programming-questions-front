import { useState, useContext } from "react";
import { useParams } from "react-router";
import { Link, useHistory } from "react-router-dom";
import { ContextoToken } from "../components/ProveedorToken";

import usePublicaciones from "../hooks/usePublicaciones";

const EditarPublicacionRespuestas = () => {
  const [publicaciones] = usePublicaciones();
  const [token] = useContext(ContextoToken);
  let history = useHistory();
  const { id } = useParams();
  const [respuesta, setRespuesta] = useState(publicaciones[id]?.textoRespuesta);

  const editarRespuestas = async (e) => {
    if (respuesta) {
      const res = await fetch(`http://localhost:3050/pregunta/respuesta/${publicaciones[id].id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          autorizado: token,
        },
        body: JSON.stringify({ texto: respuesta }),
      });
      const json = await res.json();
      if (res.ok) {
        alert("se ha Editado corectamente la publicacion");
        redireccionar();
      } else {
        alert(json.error);
      }
    } else {
      alert("No has hecho ninguna modificación");
    }
  };

  const redireccionar = () => {
    history.push("/perfil/publicaciones");
  };

  return (
    <>
      <section className="editar-publicacion-pregunta">
        <article>
          <Link className="Link" to={`/pregunta/${publicaciones[id]?.id_pregunta}`}>
            <h2 className="preguntaID">{publicaciones[id]?.titulo}</h2>
          </Link>
          <p>{publicaciones[id]?.textoPregunta}</p>
        </article>
      </section>

      <section className="editar-publicacion-respuestas">
        <article>
          <p>{publicaciones[id]?.textoRespuesta}</p>
          <time>{publicaciones[id]?.fechaActualizacionRespuestas}</time>
          <time>{publicaciones[id]?.fechaPublicacionRespuestas}</time>
        </article>
      </section>

      <section>
        <form>
          <section>
            <h3>Has los cambios necesarios en la respuesta</h3>
            <textarea
              name="textarea_pregunta"
              rows="20"
              cols="100"
              placeholder="Escribe con detalle tu consulta"
              onChange={(e) => setRespuesta(e.target.value)}
              required
            ></textarea>
            <button type="button" onClick={editarRespuestas}>
              Editar Publicación
            </button>
          </section>
        </form>
      </section>
    </>
  );
};

export default EditarPublicacionRespuestas;
